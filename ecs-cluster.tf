resource "aws_ecs_cluster" "hello_world" {
  name = "hello-world-cluster"
  tags = {
    name = "terraform-ecs-cluster"
  }
}
