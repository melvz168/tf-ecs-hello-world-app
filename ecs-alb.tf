resource "aws_lb" "hello_world_lb" {
  name            = "hello-world-lb"
  subnets         = aws_subnet.public.*.id
  security_groups = [aws_security_group.lb.id]
}

resource "aws_lb_target_group" "hello_world_tg" {
  name        = "hello-world-target-group"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = aws_vpc.hello_world.id
  target_type = "ip"
}

resource "aws_lb_listener" "hello_world_listener" {
  load_balancer_arn = aws_lb.hello_world_lb.id
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_lb_target_group.hello_world_tg.id
    type             = "forward"
  }
}

# this would be added if we are going to add https listener, 
# it will also need to have a certificate coming from ACM or imported certificate
# resource "aws_lb_listener" "hello_world_external_https" {
#   load_balancer_arn = aws_lb.customers_external.arn
#   port              = "443"
#   protocol          = "HTTPS"
#   ssl_policy        = "ELBSecurityPolicy-TLS-1-2-Ext-2018-06"
#   certificate_arn   = aws_acm_certificate_validation.domain.certificate_arn

#   default_action {
#     target_group_arn = aws_lb_target_group.hello_world_tg.arn
#     type             = "forward"
#   }
# }

resource "aws_security_group" "lb" {
  name        = "hello-world-alb-security-group"
  vpc_id      = aws_vpc.hello_world.id

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }
# This would be added if we add https support
#   ingress {
#     protocol    = "tcp"
#     from_port   = 443
#     to_port     = 443
#     cidr_blocks = ["0.0.0.0/0"]
#   }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
